import React from "react";
import { useNavigate } from "react-router-dom";
import RusPriceFormat from "../../common-items/price-format";


const CatalogItem = ({ item }) => {
  const navigate = useNavigate();

  const onOrderClick = () => {
    navigate(`/catalog/${item.id}`);
  };

  return (
    <div className="col-4">
      <div className="card catalog-item-card">
        <img
          src={item.images[0]}
          className="card-img-top img-fluid"
          alt={item.title}
        />
        <div className="card-body">
          <p className="card-text">{item.title}</p>
          <p className="card-text"><RusPriceFormat priceValue={item.price}/></p>
          <button className="btn btn-outline-primary" onClick={onOrderClick}>
            Заказать
          </button>
        </div>
      </div>
    </div>
  );
};

export default CatalogItem;
