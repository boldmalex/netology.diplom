import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import catalogActions from "../../../store/store-items/catalog/catalog-actions";
import CatalogItem from "../catalog-item/catalog-item";
import Spinner from "../../common-items/spinner";
import ErrorMessage from "../../common-items/error-message";
import categoryActions from "../../../store/store-items/category/category-actions";

const CatalogItems = () => {
  const dispatch = useDispatch();
  const catalogStore = useSelector((store) => store.catalogReducer);
  const categoryStore = useSelector(store => store.categoryReducer); 

  useEffect(() => {
    loadItems();
  }, []);

  const loadItems = (isWithCategory = false) => {
    dispatch(catalogActions.catalogRequest());
    if (isWithCategory)
      dispatch(categoryActions.categoryRequest());
  };

  const loadMore = () => {
    dispatch(catalogActions.catalogOffsetRequest(catalogStore.items.length));
  };

  const showItems = () => {
    return (
      <>
        {catalogStore.items.map((item) => (
          <CatalogItem key={item.id} item={item} />
        ))}
      </>
    );
  };

  const getBtnLoadMoreClassName = () => {
    if (catalogStore.isLoadingMoreNothing) return "btn btn-invisible";
    else return "btn btn-outline-primary";
  };

  if (catalogStore.isLoading || categoryStore.isLoading) return <Spinner />;
  if (catalogStore.error || categoryStore.error) return <ErrorMessage text={"Ошибка загрузки"} btnText={"Повторить"} btnHandler={() => loadItems(true)}/>;
  if (!catalogStore.items) return null;

  return (
    <>
      <div className="row">{showItems()}</div>
      <div className="text-center">
        {catalogStore.isLoadingMore && <Spinner />}
        {catalogStore.LoadingMoreError && <ErrorMessage text={"Ошибка загрузки"} btnText={"Повторить"} btnHandler={loadMore}/>}
        {!catalogStore.isLoadingMore && !catalogStore.LoadingMoreError && 
            <button onClick={loadMore} className={getBtnLoadMoreClassName()}>
              Загрузить ещё
            </button>
        }
      </div>
    </>
  );
};

export default CatalogItems;
