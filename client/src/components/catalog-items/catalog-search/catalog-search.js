import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import catalogActions from "../../../store/store-items/catalog/catalog-actions";
import { useSelector } from "react-redux";

const CatalogSearch = () => {
  const [search, setSearch] = useState("");

  const searchTextStored = useSelector((store) => store.catalogReducer.search);
  const dispatch = useDispatch();

  useEffect(() => {
    if (searchTextStored !== "") setSearch(searchTextStored);
  }, [searchTextStored]);

  const onSubmit = (evt) => {
    evt.preventDefault();
    dispatch(catalogActions.catalogSearchRequest(search));
  };

  const onSearchChange = ({ target }) => {
    setSearch(target.value);
  };

  return (
    <form className="catalog-search-form form-inline" onSubmit={onSubmit}>
      <input
        className="form-control"
        placeholder="Поиск"
        onChange={onSearchChange}
        value={search}
      />
    </form>
  );
};

export default CatalogSearch;
