import React from "react";
import { useDispatch } from "react-redux";
import catalogActions from "../../../store/store-items/catalog/catalog-actions";

const CatalogCategoryFilterItem = ({ item, isActive }) => {
  const dispatch = useDispatch();

  const onItemClick = (evt) => {
    evt.preventDefault();
    dispatch(catalogActions.catalogCategoryRequest(item.id));
  };

  return (
    <li className="nav-item">
      <div
        className={isActive ? "nav-link active" : "nav-link"}
        onClick={onItemClick}
      >
        {item.title}
      </div>
    </li>
  );
};

export default CatalogCategoryFilterItem;
