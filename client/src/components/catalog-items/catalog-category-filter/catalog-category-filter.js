import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import categoryActions from "../../../store/store-items/category/category-actions";
import CatalogCategoryFilterItem from "./catalog-category-filter-item";

const CatalogCategoryFilter = () => {

    const dispatch = useDispatch();
    const categoryStore = useSelector(store => store.categoryReducer);
    const {categoryId} = useSelector(store => store.catalogReducer);

    useEffect(() => {
        loadCategories();
    },[]);

    const loadCategories = () =>{
        dispatch(categoryActions.categoryRequest());
    }


    if (!categoryStore.items) return null;

    return (
        <ul className="catalog-categories nav justify-content-center">
            {categoryStore.items.map(item => <CatalogCategoryFilterItem key={item.id} item={item} isActive={categoryId === item.id}/>)}
        </ul>
    )
}

export default CatalogCategoryFilter;