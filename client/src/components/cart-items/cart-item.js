import React from "react";
import RusPriceFormat from "../common-items/price-format";
import { NavLink } from "react-router-dom";
import { useDispatch } from "react-redux";
import cartActions from "../../store/store-items/cart/cart-actions";

const CartItem = ({ item }) => {
  const dispatch = useDispatch();

  const onRemoveClick = () => {
    dispatch(cartActions.cartRemove(item.innerId));
  };

  return (
    <tr>
      <td>{item.num}</td>
      <td>
        <NavLink to={`/catalog/${item.catalogItem.id}`} className="nav-link">
          {item.catalogItem.title}
        </NavLink>
      </td>
      <td>{item.selectedSize}</td>
      <td>{item.selectedCount}</td>
      <td>
        <RusPriceFormat priceValue={item.catalogItem.price} />
      </td>
      <td>
        <RusPriceFormat
          priceValue={item.catalogItem.price * item.selectedCount}
        />
      </td>
      <td>
        <button
          className="btn btn-outline-danger btn-sm"
          onClick={onRemoveClick}
        >
          Удалить
        </button>
      </td>
    </tr>
  );
};

export default CartItem;
