import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import catalogActions from "../../store/store-items/catalog/catalog-actions";

const GlobalSearch = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [isExpanded, setExpanded] = useState(false);
  const [searchText, setSearchText] = useState("");

  const onSearchSubmit = () => {
    dispatch(catalogActions.catalogSearchRequest(searchText));
    navigate("/catalog", { replace: true });
    setExpanded(!isExpanded);
    setSearchText("");
  };

  const onExpand = (evt) => {
    evt.preventDefault();
    setExpanded(!isExpanded);
  };

  const onSearchChange = ({ target }) => {
    setSearchText(target.value);
  };

  if (isExpanded)
    return (
      <>
        <form className="header-controls-search-form" onSubmit={onSearchSubmit}>
          <input
            type="text"
            name="search"
            className="form-control"
            onChange={onSearchChange}
            value={searchText}
          ></input>
        </form>
        <div
          onClick={onSearchSubmit}
          data-id="search-expander"
          className="header-controls-pic header-controls-search"
        ></div>
      </>
    );
  else
    return (
      <div
        onClick={onExpand}
        data-id="search-expander"
        className="header-controls-pic header-controls-search"
      ></div>
    );
};

export default GlobalSearch;
