import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import cartActions from "../../store/store-items/cart/cart-actions";
import ErrorMessage from "../common-items/error-message";
import Spinner from "../common-items/spinner";


const CartOrder = () => {

    const dispatch = useDispatch();

    const store = useSelector((store) => store.cartReducer);

    const [form, setForm] = useState({
        phone: "",
        address: "",
        isAgree: false
    });    
    

    const onFormChange = ({target}) => {
        const name = target.name;
        const value = target.type === "checkbox" ? target.checked : target.value;
        setForm(prevForm => ({...prevForm, [name]: value}));
    }

    const onFormSubmit = (evt) => {
        evt.preventDefault();
        createOrder();
    }

    const createOrder = () => {
      dispatch(cartActions.cartOrder(form.phone, form.address));
    }

    const isOrderBtnEnabled = () => {
        return form.phone.length > 0 && form.address.length > 0 && form.isAgree && store.items.length > 0;
    }

    const orderDoneClick = (evt) => {
        evt.preventDefault();
        dispatch(cartActions.cartOrderDone());
    }

    const showOrderDone = () => {
        return (
            <section className="order">
                <h2 className="text-center">Заказ оформлен</h2>
                <div className="wrapper text-center">
                    <button className="btn btn-outline-secondary"
                            onClick={orderDoneClick}>
                    Спасибо
                    </button>
                </div>
            </section>
        )
    }
    
    const cardStyle = {
        maxWidth: "30rem",
        margin: "0 auto",
    };

    if (store.isOrderLoading) return <Spinner/>
    if (store.orderError) return <ErrorMessage  text={"Ошибка формирования заказа"} btnText={"Повторить"} btnHandler={createOrder}/>;
    if (store.isOrderDone) return <div>{showOrderDone()}</div>

    return (
        <section className="order">
          <h2 className="text-center">Оформить заказ</h2>
          <div className="card" style={cardStyle}>
            <form className="card-body" onSubmit={onFormSubmit}>
              <div className="form-group">
                <label htmlFor="phone">Телефон</label>
                <input
                  className="form-control"
                  name="phone"
                  placeholder="Ваш телефон"
                  value={form.phone}
                  onChange={onFormChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="address">Адрес доставки</label>
                <input
                  className="form-control"
                  name="address"
                  placeholder="Адрес доставки"
                  value={form.address}
                  onChange={onFormChange}
                />
              </div>
              <div className="form-group form-check">
                <input
                  type="checkbox"
                  className="form-check-input"
                  name="isAgree"
                  checked={form.isAgree}
                  onChange={onFormChange}
                />
                <label className="form-check-label" htmlFor="agreement">
                  Согласен с правилами доставки
                </label>
              </div>
              <button type="submit" className={`btn btn-outline-secondary ${isOrderBtnEnabled() ? "" : "btn-disabled"}`}>
                Оформить
              </button>
            </form>
          </div>
        </section>
    )
}

export default CartOrder;