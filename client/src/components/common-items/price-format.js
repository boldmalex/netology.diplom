import React from "react";
import NumberFormat from "react-number-format";

const RusPriceFormat = ({priceValue}) => {
    return (
        <NumberFormat
            value = {priceValue}
            decimalSeparator = "."
            thousandSeparator= " "
            displayType="text"
            suffix=" руб."
        >
       </NumberFormat>
    )
}

export default RusPriceFormat;