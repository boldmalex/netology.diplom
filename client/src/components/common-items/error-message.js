import React from "react";

const ErrorMessage = ({ text, btnText, btnHandler }) => {
  return (
      <div className="error-wrapper text-center">
        <div className="error">
            {text}{" "}
            <button className="button-error" onClick={btnHandler}>
                {btnText}
            </button>
        </div>
    </div>
  );
};

export default ErrorMessage;
