import React from "react";
import pageItemContainer from "../../../hocs/withContainer";

const PageNotFound = ({ isWithBanner }) => {
  const renderComponent = () => {
    return (
      <section className="top-sales">
        <h2 className="text-center">Страница не найдена</h2>
        <p>Извините, такая страница не найдена!</p>
      </section>
    );
  };

  if (isWithBanner)
    return (
      <pageItemContainer.ContainerWithBanner>
        {renderComponent()}
      </pageItemContainer.ContainerWithBanner>
    );
  else
    return (
      <pageItemContainer.ContainerNormal>
        {renderComponent()}
      </pageItemContainer.ContainerNormal>
    );
};

export default PageNotFound;
