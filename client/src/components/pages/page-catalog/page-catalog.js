import React from "react";
import pageItemContainer from "../../../hocs/withContainer";
import CatalogCategoryFilter from "../../catalog-items/catalog-category-filter/catalog-category-filter";
import CatalogSearch from "../../catalog-items/catalog-search/catalog-search";
import CatalogItems from "../../catalog-items/catalog-items/catalog-items";

const PageCatalog = ({ isWithBanner, isWithSearch }) => {
  const renderComponent = () => {
    return (
      <section className="catalog">
        <h2 className="text-center">Каталог</h2>
        {isWithSearch && <CatalogSearch />}
        <CatalogCategoryFilter />
        <CatalogItems />
      </section>
    );
  };

  if (isWithBanner)
    return (
      <pageItemContainer.ContainerWithBanner>
        {renderComponent()}
      </pageItemContainer.ContainerWithBanner>
    );
  else
    return (
      <pageItemContainer.ContainerNormal>
        {renderComponent()}
      </pageItemContainer.ContainerNormal>
    );
};

export default PageCatalog;
