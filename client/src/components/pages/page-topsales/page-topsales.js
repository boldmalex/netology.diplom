import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import pageItemContainer from "../../../hocs/withContainer";
import topSalesActions from "../../../store/store-items/top-sales/topsales-actions";
import CatalogItem from "../../catalog-items/catalog-item/catalog-item";
import ErrorMessage from "../../common-items/error-message";
import Spinner from "../../common-items/spinner";

const PageTopSales = ({ isWithBanner }) => {
  const dispatch = useDispatch();
  const { items, isLoading, error } = useSelector(
    (store) => store.topSalesReducer
  );

  useEffect(() => {
    loadItems();
  }, []);

  const loadItems = () => {
    dispatch(topSalesActions.topSalesRequest());
  };

  const showItems = () => {
    return (
      <>
        {items.map((item) => (
          <CatalogItem key={item.id} item={item} />
        ))}
      </>
    );
  };

  const renderComponent = () => {
    return (
      <section className="top-sales">
        <h2 className="text-center">Хиты продаж!</h2>
        <div className="row">{showItems()}</div>
      </section>
    );
  };

  if (isLoading) return <Spinner />;
  if (error) return <ErrorMessage text={"Ошибка загрузки"} btnText={"Повторить"} btnHandler={loadItems}/>;
  if (!items) return null;

  if (isWithBanner)
    return (
      <pageItemContainer.ContainerWithBanner>
        {renderComponent()}
      </pageItemContainer.ContainerWithBanner>
    );
  else
    return (
      <pageItemContainer.ContainerNormal>
        {renderComponent()}
      </pageItemContainer.ContainerNormal>
    );
};

export default PageTopSales;
