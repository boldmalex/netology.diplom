import React from "react";
import PageCatalog from "../page-catalog/page-catalog";
import pageItemContainer from "../../../hocs/withContainer";
import PageTopSales from "../page-topsales/page-topsales";

const PageMain = ({ isWithBanner }) => {
  const renderComponent = () => {
    return (
      <>
        <PageTopSales />
        <PageCatalog />
      </>
    );
  };

  if (isWithBanner)
    return (
      <pageItemContainer.ContainerWithBanner>
        {renderComponent()}
      </pageItemContainer.ContainerWithBanner>
    );
  else
    return (
      <pageItemContainer.ContainerNormal>
        {renderComponent()}
      </pageItemContainer.ContainerNormal>
    );
};

export default PageMain;
