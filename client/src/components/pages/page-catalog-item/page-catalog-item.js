import React, { useEffect, useState } from "react";
import pageItemContainer from "../../../hocs/withContainer";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import catalogItemActions from "../../../store/store-items/catalog-item/catalogitem-actions";
import { useSelector } from "react-redux";
import Spinner from "../../common-items/spinner";
import cartActions from "../../../store/store-items/cart/cart-actions";
import CartDataModel from "../../../store/store-items/cart/cart-data-model";
import ErrorMessage from "../../common-items/error-message";

const PageCatalogItem = ({ isWithBanner }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { id: itemId } = useParams();
  const { item, isLoading, error } = useSelector((store) => store.catalogItemReducer);
  const [count, setCount] = useState(1);
  const [selectedSize, setSelectedSize] = useState("");

  useEffect(() => {
    loadItem();
  }, []);

  useEffect(() => {
    if (item) setSelectedSize(item.sizes[0].size);
  }, [item]);

  const loadItem = () => {
    dispatch(catalogItemActions.catalogItemRequest(itemId));
  };

  const onChangeCount = (delta) => {
    setCount((lastCount) => (lastCount + delta > 0 ? lastCount + delta : 1));
  };

  const onSizeClick = ({ target }) => {
    setSelectedSize(target.textContent);
  };

  const onToCartClick = () => {
    dispatch(cartActions.cartAdd(new CartDataModel(item, selectedSize, count)));
    navigate("/cart");
  }

  const showSizes = (sizes) => {
    return sizes.map((sizeItem) => showSize(sizeItem));
  };

  const showSize = (sizeItem) => {
    return (
        <span
          key={sizeItem.size}
          className={
            selectedSize === sizeItem.size
              ? "catalog-item-size selected"
              : "catalog-item-size"
          }
          onClick={onSizeClick}
        >
          {sizeItem.size}
        </span>
    );
  };

  const renderComponent = () => {
    if (isLoading) return <Spinner />;
    if (error) return <ErrorMessage text={"Ошибка загрузки"} btnText={"Повторить"} btnHandler={loadItem}/>;
    if (!item) return null;
    return (
      <section className="catalog-item">
        <h2 className="text-center">{item.title}</h2>
        <div className="row">
          <div className="col-5">
            <img src={item.images[0]} className="img-fluid" alt="" />
          </div>
          <div className="col-7">
            <table className="table table-bordered">
              <tbody>
                <tr>
                  <td>Артикул</td>
                  <td>{item.sku}</td>
                </tr>
                <tr>
                  <td>Производитель</td>
                  <td>{item.manufacturer}</td>
                </tr>
                <tr>
                  <td>Цвет</td>
                  <td>{item.color}</td>
                </tr>
                <tr>
                  <td>Материалы</td>
                  <td>{item.material}</td>
                </tr>
                <tr>
                  <td>Сезон</td>
                  <td>{item.season}</td>
                </tr>
                <tr>
                  <td>Повод</td>
                  <td>{item.reason}</td>
                </tr>
              </tbody>
            </table>
            <div className="text-center">
              <p>
                Размеры в наличии: {showSizes(item.sizes)}
              </p>
              <p>
                Количество:{" "}
                <span className="btn-group btn-group-sm pl-2">
                  <button
                    className="btn btn-secondary"
                    onClick={() => onChangeCount(-1)}
                  >
                    -
                  </button>
                  <span className="btn btn-outline-primary">{count}</span>
                  <button
                    className="btn btn-secondary"
                    onClick={() => onChangeCount(1)}
                  >
                    +
                  </button>
                </span>
              </p>
            </div>
            <button 
              className="btn btn-danger btn-block btn-lg"
              onClick={onToCartClick}
              >
              В корзину
            </button>
          </div>
        </div>
      </section>
    );
  };

  if (isWithBanner)
    return (
      <pageItemContainer.ContainerWithBanner>
        {renderComponent()}
      </pageItemContainer.ContainerWithBanner>
    );
  else
    return (
      <pageItemContainer.ContainerNormal>
        {renderComponent()}
      </pageItemContainer.ContainerNormal>
    );
};

export default PageCatalogItem;
