import React, {useEffect} from "react";
import { useSelector, useDispatch } from "react-redux";
import pageItemContainer from "../../../hocs/withContainer";
import cartActions from "../../../store/store-items/cart/cart-actions";
import CartItem from "../../cart-items/cart-item";
import Spinner from "../../common-items/spinner";
import RusPriceFormat from "../../common-items/price-format";
import CartOrder from "../../cart-order/cart-order";


const PageCart = ({ isWithBanner }) => {
  
  const dispatch = useDispatch();
  const store = useSelector((store) => store.cartReducer);

  useEffect(() => {
    loadItems();
  }, []);

  const loadItems = () => {
    dispatch(cartActions.cartRequest());
  };

  const showItems = () => {
    return (
      <>
        {store.items.map((item) => (
          <CartItem key={item.innerId} item={item} />
        ))}
      </>
    );
  };

  const getFullPrice = () => {
    return store.items.map(item => item.catalogItem.price * item.selectedCount)
                      .reduce((prevItem, item) => prevItem + item, 0);
  }

  const renderComponent = () => {
    return (
      <>
        <section className="cart">
          <h2 className="text-center">Корзина</h2>
          <table className="table table-bordered">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Название</th>
                <th scope="col">Размер</th>
                <th scope="col">Кол-во</th>
                <th scope="col">Стоимость</th>
                <th scope="col">Итого</th>
                <th scope="col">Действия</th>
              </tr>
            </thead>
            <tbody>
              {showItems()}
              <tr>
                <td colSpan="5" className="text-right">
                  Общая стоимость
                </td>
                <td><RusPriceFormat priceValue={getFullPrice()}/></td>
              </tr>
            </tbody>
          </table>
        </section>
        <CartOrder/>
      </>
    );
  };

  if (store.isLoading) return <Spinner />;
  if (store.error) return <div>Error...{store.error}</div>;
  if (!store.items) return null;

  if (isWithBanner)
    return (
      <pageItemContainer.ContainerWithBanner>
        {renderComponent()}
      </pageItemContainer.ContainerWithBanner>
    );
  else
    return (
      <pageItemContainer.ContainerNormal>
        {renderComponent()}
      </pageItemContainer.ContainerNormal>
    );
};

export default PageCart;
