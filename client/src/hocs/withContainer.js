import PageBanner from "../components/page-sections/page-section-banner/page-section-banner";

const withContainer = (Component) => {
  const Wrapper = (props) => {
    const { isWithBanner } = props;

    if (isWithBanner) {
      return (
        <Banner>
          <Component {...props} />
        </Banner>
      );
    } else {
      return (
        <Normal>
          <Component {...props} />
        </Normal>
      );
    }
  };
  return Wrapper;
};

const Banner = (props) => {
  return (
    <main className="container">
      <div className="row">
        <div className="col">
          <PageBanner />
          {props.children}
        </div>
      </div>
    </main>
  );
};

const Normal = (props) => {
  return (
    <main className="container">
      <div className="row">
        <div className="col">{props.children}</div>
      </div>
    </main>
  );
};

const pageItemContainer = {
  ContainerWithBanner: withContainer(Banner),
  ContainerNormal: withContainer(Normal),
};

export default pageItemContainer;
