import { applyMiddleware, combineReducers, createStore } from "redux";
import createSagaMiddleware from "@redux-saga/core";
import catalogReducer from "./store-items/catalog/catalog-reducer";
import categoryReducer from "./store-items/category/category-reducer";
import topSalesReducer from "./store-items/top-sales/topsales-reducer";
import catalogItemReducer from "./store-items/catalog-item/catalogitem-reducer";
import cartReducer from "./store-items/cart/cart-reducer";
import rootSaga from "./sagas/root-saga";
import cartActions from "./store-items/cart/cart-actions";

const reducer = combineReducers({
  catalogReducer,
  categoryReducer,
  topSalesReducer,
  catalogItemReducer, 
  cartReducer
});

const sagaMiddlware = createSagaMiddleware();

const store = createStore(reducer, applyMiddleware(sagaMiddlware));
sagaMiddlware.run(rootSaga);
store.dispatch(cartActions.cartRequest());

export default store;
