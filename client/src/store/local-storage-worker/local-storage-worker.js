const loadState = (key) => {
  return new Promise(function (resolve, reject) {
    try {
      const serializedState = localStorage.getItem(key);
      if (serializedState === null) {
        resolve(null);
      }
      resolve(JSON.parse(serializedState));
    } catch (e) {
      reject(e);
    }
  });
};

const saveState = (key, state) => {
  return new Promise(function (resolve, reject) {
    try {
      let serializedState = JSON.stringify(state);
      localStorage.setItem(key, serializedState);
      resolve();
    } catch (e) {
      reject(e);
    }
  });
};

const clearState = (key) => {
  console.log('start clear state');
  return new Promise(function(resolve, reject){
    try {
      localStorage.removeItem(key);
      console.log('cleared state');
      resolve();
    }
    catch (e){
      reject(e);
    }
  });
};

const localStorageWorker = {
  loadState,
  saveState,
  clearState
};

export default localStorageWorker;
