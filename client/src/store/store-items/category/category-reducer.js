import categoryActionTypes from "./category-action-types";

const initialState = {
  items: [],
  isLoading: false,
  error: null,
  defaultItems: [{ id: 0, title: "Все" }],
};

const categoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case categoryActionTypes.CATEGORY_REQUEST:
      return { 
        ...state, 
        isLoading: true, 
        error: null 
    };

    case categoryActionTypes.CATEGORY_SUCCESS:
      const { items } = action.payload;
      return {
        ...state,
        isLoading: false,
        error: null,
        items: [...state.defaultItems, ...items],
      };

    case categoryActionTypes.CATEGORY_FAILURE:
      const { error } = action.payload;
      return { 
        ...state, 
        isLoading: false, 
        error: error 
    };

    default:
      return state;
  }
};

export default categoryReducer;
