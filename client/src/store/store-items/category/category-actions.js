import categoryActionTypes from "./category-action-types";

const categoryRequest = () => ({
  type: categoryActionTypes.CATEGORY_REQUEST,
});

const categorySuccess = (items) => ({
  type: categoryActionTypes.CATEGORY_SUCCESS,
  payload: { items },
});

const categoryFailure = (error) => ({
  type: categoryActionTypes.CATEGORY_FAILURE,
  payload: { error },
});

const categoryActions = {
  categoryRequest,
  categorySuccess,
  categoryFailure,
};

export default categoryActions;
