import topSalesActionTypes from "./topsales-action-types";

const initialState = {
  items: [],
  isLoading: false,
  error: null,
};

const topSalesReducer = (state = initialState, action) => {
  switch (action.type) {
    case topSalesActionTypes.TOPSALES_REQUEST:
      return { 
        ...state, 
        isLoading: true, 
        error: null 
    };

    case topSalesActionTypes.TOPSALES_SUCCESS:
      const { items } = action.payload;
      return { 
        ...state, 
        isLoading: false, 
        error: null, 
        items: [...items] 
    };

    case topSalesActionTypes.TOPSALES_FAILURE:
      const { error } = action.payload;
      return { 
        ...state, 
        isLoading: false, 
        error: error 
    };

    default:
      return state;
  }
};

export default topSalesReducer;
