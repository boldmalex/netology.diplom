import topSalesActionTypes from "./topsales-action-types";

const topSalesRequest = () => ({
  type: topSalesActionTypes.TOPSALES_REQUEST,
});

const topSalesSuccess = (items) => ({
  type: topSalesActionTypes.TOPSALES_SUCCESS,
  payload: { items },
});

const topSalesFailure = (error) => ({
  type: topSalesActionTypes.TOPSALES_FAILURE,
  payload: { error },
});

const topSalesActions = {
  topSalesRequest,
  topSalesSuccess,
  topSalesFailure,
};

export default topSalesActions;
