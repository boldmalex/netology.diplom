import catalogActionTypes from "./catalog-action-types";

const catalogRequest = (offset) => ({
  type: catalogActionTypes.CATALOG_REQUEST,
  payload: { offset },
});

const catalogSuccess = (items) => ({
  type: catalogActionTypes.CATALOG_SUCCESS,
  payload: { items },
});

const catalogFailure = (error) => ({
  type: catalogActionTypes.CATALOG_FAILURE,
  payload: { error },
});

const catalogOffsetRequest = (offset) => ({
  type: catalogActionTypes.CATALOG_OFFSET_REQUEST,
  payload: { offset },
});

const catalogOffsetSuccess = (items) => ({
  type: catalogActionTypes.CATALOG_OFFSET_SUCCESS,
  payload: { items },
});
const catalogOffsetFailure = (error) => ({
  type: catalogActionTypes.CATALOG_OFFSET_FAILURE,
  payload: { error },
});

const catalogCategoryRequest = (categoryId) => ({
  type: catalogActionTypes.CATALOG_CATEGORY_REQUEST,
  payload: { categoryId },
});

const catalogCategorySuccess = (items) => ({
  type: catalogActionTypes.CATALOG_CATEGORY_SUCCESS,
  payload: { items },
});

const catalogCategoryFailure = (error) => ({
  type: catalogActionTypes.CATALOG_CATEGORY_FAILURE,
  payload: { error },
});

const catalogSearchRequest = (search) => ({
  type: catalogActionTypes.CATALOG_SEARCH_REQUEST,
  payload: { search },
});

const catalogActions = {
  catalogRequest,
  catalogSuccess,
  catalogFailure,
  catalogOffsetRequest,
  catalogOffsetSuccess,
  catalogOffsetFailure,
  catalogCategoryRequest,
  catalogCategorySuccess,
  catalogCategoryFailure,
  catalogSearchRequest,
};

export default catalogActions;
