import catalogActionTypes from "./catalog-action-types";

const initialState = {
  items: [],
  isLoading: false,
  error: null,
  categoryId: 0,
  search: "",
  isLoadingMore: false,
  LoadingMoreError: null,
  isLoadingMoreNothing: false,
};

const catalogReducer = (state = initialState, action) => {
  switch (action.type) {
    case catalogActionTypes.CATALOG_REQUEST:
      return {
        ...state,
        isLoading: true,
        error: null,
        isLoadingMoreNothing: false,
        LoadingMoreError: null
      };

    case catalogActionTypes.CATALOG_SUCCESS:
      const { items } = action.payload;
      return { 
        ...state, 
        isLoading: false, 
        error: null, 
        items: items 
    };

    case catalogActionTypes.CATALOG_FAILURE:
      const { error } = action.payload;
      return { 
        ...state, 
        isLoading: false, 
        error: error 
    };

    case catalogActionTypes.CATALOG_OFFSET_REQUEST:
      const { offset } = action.payload;
      return {
        ...state,
        offset: offset,
        isLoadingMore: true,
        isLoadingMoreNothing: false,
        LoadingMoreError:null
      };

    case catalogActionTypes.CATALOG_OFFSET_SUCCESS:
      const { items: offsetItems } = action.payload;
      return {
        ...state,
        isLoadingMore: false,
        error: null,
        items: [...state.items, ...offsetItems],
        isLoadingMoreNothing: offsetItems.length === 0,
      };

    case catalogActionTypes.CATALOG_OFFSET_FAILURE:
      const { error: offsetError } = action.payload;
      return { 
        ...state, 
        isLoadingMore: false,
        LoadingMoreError: offsetError 
    };

    case catalogActionTypes.CATALOG_CATEGORY_REQUEST:
      const { categoryId } = action.payload;
      return {
        ...state,
        isLoading: true,
        categoryId: categoryId,
        isLoadingMoreNothing: false,
        LoadingMoreError: null
      };

    case catalogActionTypes.CATALOG_CATEGORY_SUCCESS:
      const { items: categoryItems } = action.payload;
      return { 
        ...state, 
        isLoading: false, 
        error: null, 
        items: categoryItems 
    };

    case catalogActionTypes.CATALOG_CATEGORY_FAILURE:
      const { error: categoryError } = action.payload;
      return { 
        ...state, 
        isLoading: false, 
        error: categoryError 
    };

    case catalogActionTypes.CATALOG_SEARCH_REQUEST:
      const { search } = action.payload;
      return {
        ...state,
        isLoading: true,
        search: search,
        isLoadingMoreNothing: false,
        LoadingMoreError: null
      };

    default:
      return state;
  }
};

export default catalogReducer;
