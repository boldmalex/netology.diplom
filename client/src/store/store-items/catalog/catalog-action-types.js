const CATALOG_REQUEST = "CATALOG_REQUEST";
const CATALOG_SUCCESS = "CATALOG_SUCCESS";
const CATALOG_FAILURE = "CATALOG_FAILURE";
const CATALOG_OFFSET_REQUEST = "CATALOG_OFFSET_REQUEST";
const CATALOG_OFFSET_SUCCESS = "CATALOG_OFFSET_SUCCESS";
const CATALOG_OFFSET_FAILURE = "CATALOG_OFFSET_FAILURE";
const CATALOG_CATEGORY_REQUEST = "CATALOG_CATEGORY_REQUEST";
const CATALOG_CATEGORY_SUCCESS = "CATALOG_CATEGORY_SUCCESS";
const CATALOG_CATEGORY_FAILURE = "CATALOG_CATEGORY_FAILURE";
const CATALOG_SEARCH_REQUEST = "CATALOG_SEARCH_REQUEST";


const catalogActionTypes = {
  CATALOG_REQUEST,
  CATALOG_SUCCESS,
  CATALOG_FAILURE,
  CATALOG_OFFSET_REQUEST,
  CATALOG_OFFSET_SUCCESS,
  CATALOG_OFFSET_FAILURE,
  CATALOG_CATEGORY_REQUEST,
  CATALOG_CATEGORY_SUCCESS,
  CATALOG_CATEGORY_FAILURE,
  CATALOG_SEARCH_REQUEST,
};

export default catalogActionTypes;
