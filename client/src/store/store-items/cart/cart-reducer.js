import cartActionTypes from "./cart-action-types";

const initialState = {
  items: [],
  isLoading: false,
  error: null,
  changeError: null,
  isOrderLoading: false,
  orderError: null,
  isOrderDone: false
};

const cartReducer = (state = initialState, action) => {
  
  switch (action.type) {
  
    case cartActionTypes.CART_REQUEST:
      return {
        ...state,
        isLoading: true,
        error: null,
        isOrderDone: false
      };

    case cartActionTypes.CART_REQUEST_SUCCESS:
      const { items } = action.payload;
      return {
        ...state,
        isLoading: false,
        error: null,
        items: items === null ? [] : [...items],
      };

    case cartActionTypes.CART_REQUEST_FAILURE:
      const { error } = action.payload;
      return {
        ...state,
        isLoading: false,
        error: error,
      };

    case cartActionTypes.CART_ADD:
      const { item: inputItem } = action.payload;
      const existedItem = state.items.find(
        (i) => i.innerId === inputItem.innerId
      );
      
      if (!existedItem) {
        inputItem.num = state.items.length + 1;
        return {
          ...state,
          items: [...state.items, inputItem],
        };
      } else {
        const itemIndex = state.items.findIndex(
          (i) => i.innerId === inputItem.innerId
        );
        existedItem.selectedCount += inputItem.selectedCount;
        return {
          ...state,
          items: [
            ...state.items.slice(0, itemIndex),
            existedItem,
            ...state.items.slice(itemIndex + 1, state.items.length),
          ],
        };
      }

    case cartActionTypes.CART_REMOVE:
      const { id } = action.payload;
      const itemIndex = state.items.findIndex((i) => i.innerId === id);
      if (itemIndex === -1) return state;
      else {
        const newItems = [
          ...state.items.slice(0, itemIndex),
          ...state.items.slice(itemIndex + 1, state.items.length),
        ];
        const renumberedItems = newItems.map((item, index) => {
          return { ...item, num: index + 1 };
        });
        return {
          ...state,
          items: renumberedItems,
        };
      }

    case cartActionTypes.CART_CHANGE_FAILURE:
      const { error: changeError } = action.payload;
      return {
        ...state,
        changeError: changeError,
      };

    case cartActionTypes.CART_ORDER:
      return {
        ...state,
        isOrderLoading: true,
        isOrderError: null,
      };

    case cartActionTypes.CART_ORDER_SUCCESS:
      return {
        ...state,
        items: [],
        isOrderLoading: false,
        isOrderError: null,
        isOrderDone: true
      };

    case cartActionTypes.CART_ORDER_FAILURE:
      const { error: orderError } = action.payload;
      return {
        ...state,
        isOrderLoading: false,
        isOrderError: orderError,
      };

    case cartActionTypes.CART_ORDER_DONE:
      return {
        ...state,
        isOrderDone: false
      };

    default:
      return state;
  }
};

export default cartReducer;
