class OrderDataModel {
    constructor(owner, items) {
        this.owner = owner;
        this.items = items;
    }
}

export class OrderDataItemModel {
    constructor(id, price, size, count) {
        this.id = id;
        this.price = price;
        this.size = size;
        this.count = count;
    }
}

export class OrderOwnerModel {
    constructor(phone, address) {
        this.phone = phone;
        this.address = address;
    }
}

export default OrderDataModel;