class CartDataModel {
    constructor(catalogItem, selectedSize, selectedCount, num) {
        this.catalogItem = catalogItem;
        this.selectedSize = selectedSize;
        this.selectedCount = selectedCount;
        this.innerId = `${catalogItem.id}_${selectedSize}`;
        this.num = num;
    }
}

export default CartDataModel;