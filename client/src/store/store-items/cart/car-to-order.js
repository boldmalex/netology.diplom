import OrderDataModel, {OrderDataItemModel, OrderOwnerModel} from "./order-data-model"

const cartToOrder = (phone, address, storeItems) => {
    const owner = new OrderOwnerModel(phone, address);
    const order = new OrderDataModel(owner, [])
    storeItems.forEach(item => {
        const orderItem = new OrderDataItemModel(item.catalogItem.id, item.catalogItem.price, item.selectedSize, item.selectedCount);
        order.items.push(orderItem);
    });
    return order;
}

export default cartToOrder;