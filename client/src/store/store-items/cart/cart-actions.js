import cartActionTypes from "./cart-action-types";

const cartRequest = () => ({
  type: cartActionTypes.CART_REQUEST,
});

const cartRequestSuccess = (items) => ({
  type: cartActionTypes.CART_REQUEST_SUCCESS,
  payload: { items },
});

const cartRequestFailure = (error) => ({
  type: cartActionTypes.CART_REQUEST_FAILURE,
  payload: { error },
});

const cartAdd = (item) => ({
  type: cartActionTypes.CART_ADD,
  payload: { item },
});

const cartRemove = (id) => ({
  type: cartActionTypes.CART_REMOVE,
  payload: { id },
});

const cartChangeFailure = (error) => ({
  type: cartActionTypes.CART_CHANGE_FAILURE,
  payload: { error },
});

const cartOrder = (phone, address) => ({
  type: cartActionTypes.CART_ORDER,
  payload: { phone, address },
});

const cartOrderSuccess = () => ({
  type: cartActionTypes.CART_ORDER_SUCCESS,
});

const cartOrderFailure = (error) => ({
  type: cartActionTypes.CART_ORDER_FAILURE,
  payload: { error },
});

const cartOrderDone = () => ({
  type: cartActionTypes.CART_ORDER_DONE,
});


const cartActions = {
    cartRequest,
    cartRequestSuccess,
    cartRequestFailure,
    cartAdd,
    cartRemove,
    cartChangeFailure,
    cartOrder,
    cartOrderSuccess,
    cartOrderFailure,
    cartOrderDone
};

export default cartActions;