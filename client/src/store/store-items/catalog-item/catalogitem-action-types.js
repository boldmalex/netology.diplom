const CATALOG_ITEM_REQUEST = "CATALOG_ITEM_REQUEST";
const CATALOG_ITEM_SUCCESS = "CATALOG_ITEM_SUCCESS";
const CATALOG_ITEM_FAILURE = "CATALOG_ITEM_FAILURE";


const catalogItemActionTypes = {
  CATALOG_ITEM_REQUEST,
  CATALOG_ITEM_SUCCESS,
  CATALOG_ITEM_FAILURE,
};

export default catalogItemActionTypes;