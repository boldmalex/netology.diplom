import catalogItemActionTypes from "./catalogitem-action-types";

const catalogItemRequest = (itemId) => ({
  type: catalogItemActionTypes.CATALOG_ITEM_REQUEST,
  payload: { itemId },
});

const catalogItemSuccess = (item) => ({
  type: catalogItemActionTypes.CATALOG_ITEM_SUCCESS,
  payload: { item },
});

const catalogItemFailure = (error) => ({
  type: catalogItemActionTypes.CATALOG_ITEM_FAILURE,
  payload: { error },
});


const catalogItemActions = {
  catalogItemRequest,
  catalogItemSuccess,
  catalogItemFailure,
};

export default catalogItemActions;