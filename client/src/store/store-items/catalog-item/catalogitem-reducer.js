import catalogItemActionTypes from "./catalogitem-action-types";

const initialState = {
  item: null,
  isLoading: false,
  error: null
};

const catalogItemReducer = (state = initialState, action) => {
  switch (action.type) {
    case catalogItemActionTypes.CATALOG_ITEM_REQUEST:
      return { 
        ...state, 
        isLoading: true, 
        error: null 
    };

    case catalogItemActionTypes.CATALOG_ITEM_SUCCESS:
      const { item } = action.payload;
      return {
        ...state,
        isLoading: false,
        error: null,
        item: item
      };

    case catalogItemActionTypes.CATALOG_ITEM_FAILURE:
      const { error } = action.payload;
      return { 
        ...state, 
        isLoading: false, 
        error: error 
    };

    default:
      return state;
  }
};

export default catalogItemReducer;
