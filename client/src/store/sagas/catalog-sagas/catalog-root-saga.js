import { spawn } from "redux-saga/effects";
import {
  watcherCatalogRequestSaga,
  watcherCatalogOffsetSaga,
  watcherCatalogCategorySaga,
  watcherCatalogSearchSaga,
} from "./catalog-saga";

export default function* catalogRootSaga() {
  yield spawn(watcherCatalogRequestSaga);
  yield spawn(watcherCatalogOffsetSaga);
  yield spawn(watcherCatalogCategorySaga);
  yield spawn(watcherCatalogSearchSaga);
}
