import { call, put, select, takeLeading } from "redux-saga/effects";
import catalogActionTypes from "../../store-items/catalog/catalog-action-types";
import catalogActions from "../../store-items/catalog/catalog-actions";
import apiServiceCatalog from "../../../api-services/api-service-catalog";

export function* watcherCatalogRequestSaga() {
  yield takeLeading(
    catalogActionTypes.CATALOG_REQUEST,
    workerCatalogRequestSaga
  );
}

function* workerCatalogRequestSaga(action) {
  try {
    const { categoryId, search } = yield select(
      (store) => store.catalogReducer
    );
    const items = yield call(
      apiServiceCatalog.catalogGet,
      categoryId,
      action.payload.offset,
      search
    );
    yield put(catalogActions.catalogSuccess(items));
  } catch (e) {
    yield put(catalogActions.catalogFailure(e));
  }
}

export function* watcherCatalogOffsetSaga() {
  yield takeLeading(
    catalogActionTypes.CATALOG_OFFSET_REQUEST,
    workerCatalogOffsetSaga
  );
}

function* workerCatalogOffsetSaga(action) {
  try {
    const { categoryId, search } = yield select(
      (store) => store.catalogReducer
    );
    const items = yield call(
      apiServiceCatalog.catalogGet,
      categoryId,
      action.payload.offset,
      search
    );
    yield put(catalogActions.catalogOffsetSuccess(items));
  } catch (e) {
    yield put(catalogActions.catalogOffsetFailure(e));
  }
}

export function* watcherCatalogCategorySaga() {
  yield takeLeading(
    catalogActionTypes.CATALOG_CATEGORY_REQUEST,
    workerCatalogCategorySaga
  );
}

function* workerCatalogCategorySaga(action) {
  try {
    const { search } = yield select(
      (store) => store.catalogReducer
    );

    const items = yield call(
      apiServiceCatalog.catalogGet,
      action.payload.categoryId,
      action.payload.offset,
      search
    );
    yield put(catalogActions.catalogCategorySuccess(items));
  } catch (e) {
    yield put(catalogActions.catalogCategoryFailure(e));
  }
}

export function* watcherCatalogSearchSaga() {
  yield takeLeading(
    catalogActionTypes.CATALOG_SEARCH_REQUEST,
    workerCatalogSearchSaga
  );
}

function* workerCatalogSearchSaga(action) {
  try {
    
    const { categoryId } = yield select(
      (store) => store.catalogReducer
    );

    const items = yield call(
      apiServiceCatalog.catalogGet,
      categoryId,
      0,
      action.payload.search
    );
    yield put(catalogActions.catalogSuccess(items));
  } catch (e) {
    yield put(catalogActions.catalogFailure(e));
  }
}
