import { call, put, takeLeading, spawn } from "redux-saga/effects";
import catalogItemActions from "../../store-items/catalog-item/catalogitem-actions";
import apiServiceCatalog from "../../../api-services/api-service-catalog";
import catalogItemActionTypes from "../../store-items/catalog-item/catalogitem-action-types";

function* watcherCatalogItemRequestSaga() {
  yield takeLeading(
    catalogItemActionTypes.CATALOG_ITEM_REQUEST,
    workerCatalogItemRequestSaga
  );
}

function* workerCatalogItemRequestSaga(action) {
  try {
    const item = yield call(apiServiceCatalog.catalogItemGet, action.payload.itemId);
    yield put(catalogItemActions.catalogItemSuccess(item));
  } catch (e) {
    yield put(catalogItemActions.catalogItemFailure(e));
  }
}

export default function* catalogItemRootSaga() {
  yield spawn(watcherCatalogItemRequestSaga);
}