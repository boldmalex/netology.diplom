import { call, put, takeLeading, spawn, select } from "redux-saga/effects";
import cartActionTypes from "../../store-items/cart/cart-action-types";
import cartActions from "../../store-items/cart/cart-actions";
import localStorageWorker from "../../local-storage-worker/local-storage-worker";
import apiServiceOrder from "../../../api-services/api-service-order";
import cartToOrder from "../../store-items/cart/car-to-order";


const cartStateKey = "cart";

function* watcherCartRequestSaga() {
  yield takeLeading(
    cartActionTypes.CART_REQUEST,
    workerCartRequestSaga
  );
}

function* workerCartRequestSaga(action) {
  try {
    const items = yield call(localStorageWorker.loadState, cartStateKey);
    yield put(cartActions.cartRequestSuccess(items));
  } catch (e) {
    yield put(cartActions.cartRequestFailure(e));
  }
}

function* watcherCartAddSaga() {
    yield takeLeading(
      cartActionTypes.CART_ADD,
      workerCartChangeSaga
    );
}

function* watcherCartRemoveSaga() {
    yield takeLeading(
      cartActionTypes.CART_REMOVE,
      workerCartChangeSaga
    );
}
  
function* workerCartChangeSaga(action) {
    try {
        const { items } = yield select(
            (store) => store.cartReducer
          );
      yield call(localStorageWorker.saveState, cartStateKey, items);
    } catch (e) {
      yield put(cartActions.cartChangeFailure(e));
    }
}

function* watcherCartOrderSaga() {
  yield takeLeading(
    cartActionTypes.CART_ORDER,
    workerCartOrderSaga
  );
}

function* workerCartOrderSaga(action) {
  try {
      const { items } = yield select(
          (store) => store.cartReducer
        );
      const {phone, address} = action.payload;
      const order = cartToOrder(phone, address, items);

    yield call(apiServiceOrder.orderPut, order);
    console.log('order put');
    yield call(localStorageWorker.clearState, cartStateKey);
    console.log('clear state');
    yield put(cartActions.cartOrderSuccess())
    console.log('put success');
  } catch (e) {
    yield put(cartActions.cartOrderFailure(e));
  }
}


export default function* cartRootSaga() {
  yield spawn(watcherCartRequestSaga);
  yield spawn(watcherCartAddSaga);
  yield spawn(watcherCartRemoveSaga);
  yield spawn(watcherCartOrderSaga);
}