import { call, put, takeLeading, spawn } from "redux-saga/effects";
import categoryActions from "../../store-items/category/category-actions";
import apiServiceCategory from "../../../api-services/api-service-category";
import categoryActionTypes from "../../store-items/category/category-action-types";

function* watcherCategoryRequestSaga() {
  yield takeLeading(
    categoryActionTypes.CATEGORY_REQUEST,
    workerCategoryRequestSaga
  );
}

function* workerCategoryRequestSaga(action) {
  try {
    const items = yield call(apiServiceCategory.categoryGet);
    yield put(categoryActions.categorySuccess(items));
  } catch (e) {
    yield put(categoryActions.categoryFailure(e));
  }
}

export default function* categoryRootSaga() {
  yield spawn(watcherCategoryRequestSaga);
}
