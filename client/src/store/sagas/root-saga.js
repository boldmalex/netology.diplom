import { spawn } from "redux-saga/effects";
import catalogRootSaga from "./catalog-sagas/catalog-root-saga";
import categoryRootSaga from "./category-saga/category-root-saga";
import topSalesRootSaga from "./topsales-saga/topsales-root-saga";
import catalogItemRootSaga from "./catalogitem-saga/catalogitem-root-saga";
import cartRootSaga from "./cart-sagas/cart-root-saga";


export default function* rootSaga() {
  yield spawn(catalogRootSaga);
  yield spawn(categoryRootSaga);
  yield spawn(topSalesRootSaga);
  yield spawn(catalogItemRootSaga);
  yield spawn(cartRootSaga);
}
