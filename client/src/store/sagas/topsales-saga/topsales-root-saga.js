import { call, put, takeLeading, spawn } from "redux-saga/effects";
import topSalesActions from "../../store-items/top-sales/topsales-actions";
import apiServiceTopSales from "../../../api-services/api-service-topsales";
import topSalesActionTypes from "../../store-items/top-sales/topsales-action-types";

function* watcherTopSalesRequestSaga() {
  yield takeLeading(
    topSalesActionTypes.TOPSALES_REQUEST,
    workerTopSalesRequestSaga
  );
}

function* workerTopSalesRequestSaga(action) {
  try {
    const items = yield call(apiServiceTopSales.topSalesGet);
    yield put(topSalesActions.topSalesSuccess(items));
  } catch (e) {
    yield put(topSalesActions.topSalesFailure(e));
  }
}

export default function* topSalesRootSaga() {
  yield spawn(watcherTopSalesRequestSaga);
}
