import react from "react";
import PageItemFooter from "./components/page-sections/page-section-footer/page-section-footer";
import PageItemHeader from "./components/page-sections/page-section-header/page-section-header";
import PageMain from "./components/pages/page-main/page-main";
import PageAbout from "./components/pages/page-about/page-about";
import PageCart from "./components/pages/page-cart/page-cart";
import PageCatalog from "./components/pages/page-catalog/page-catalog";
import PageContacts from "./components/pages/page-contacts/page-contacts";
import PageNotFound from "./components/pages/page-notfound/page-notfound";
import PageCatalogItem from "./components/pages/page-catalog-item/page-catalog-item";

import { Route, Routes, BrowserRouter as Router } from "react-router-dom";
import "./App.css";
import { Provider } from "react-redux";
import store from "./store/store";


function App() {
  return (
    <Provider store={store}>
      <Router>
        <PageItemHeader />
        <Routes>
          <Route path="/" element={<PageMain isWithBanner={true} />} />
          <Route path="/catalog" element={<PageCatalog isWithBanner={true} isWithSearch={true} />}/>
          <Route path="/about" element={<PageAbout isWithBanner={true} />} />
          <Route path="/cart" element={<PageCart isWithBanner={true} />} />
          <Route path="/contacts" element={<PageContacts isWithBanner={true} />}/>
          <Route path="/catalog/:id" element={<PageCatalogItem  isWithBanner={true}/>}/>
          <Route path="*" element={<PageNotFound isWithBanner={true} />} />
        </Routes>
        <PageItemFooter />
      </Router>
    </Provider>
  );
}

export default App;
