const url = "http://localhost:7070/api/order";

const orderPut = async (order) => {
  console.log("orderPut", order);
  const response = await fetch(`${url}`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(order),
  });
  console.log("orderPut response", response);
  if (!response.ok) {
    throw new Error(response.status);
  }
};

const apiServiceOrder = {
  orderPut,
};

export default apiServiceOrder;
