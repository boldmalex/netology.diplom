const url = "http://localhost:7070/api/items";

const catalogGet = async (categoryId = 0, offset = 0, search = "") => {
  const query = new URLSearchParams({
    categoryId: categoryId,
    offset: offset,
    q: search,
  });
  const response = await fetch(`${url}?${query}`);
  if (!response.ok) {
    throw new Error(response.status);
  }
  return await response.json();
};

const catalogItemGet = async (id) => {
  const response = await fetch(`${url}/${id}`);
  if (!response.ok) throw new Error(response.status);
  return await response.json();
};

const apiServiceCatalog = {
  catalogGet,
  catalogItemGet,
};

export default apiServiceCatalog;
