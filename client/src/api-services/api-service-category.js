const url = "http://localhost:7070/api/categories";

const categoryGet = async () => {
  const response = await fetch(`${url}`);
  if (!response.ok) {
    throw new Error(response.status);
  }
  return await response.json();
};

const apiServiceCategory = {
  categoryGet,
};

export default apiServiceCategory;
