const url = "http://localhost:7070/api/top-sales";

const topSalesGet = async () => {
  const response = await fetch(`${url}`);
  if (!response.ok) {
    throw new Error(response.status);
  }
  return await response.json();
};

const apiServiceTopSales = {
  topSalesGet,
};

export default apiServiceTopSales;
